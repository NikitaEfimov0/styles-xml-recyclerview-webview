package com.example.task13

import android.annotation.SuppressLint
import android.content.ContentValues.TAG
import android.content.Context
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebViewClient
import androidx.activity.OnBackPressedCallback
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import com.example.task13.databinding.FragmentWebBinding


class WebFragment : Fragment() {
    lateinit var urlFromUser:String
    lateinit var binding: FragmentWebBinding
    lateinit var interf:OpenNew
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentWebBinding.inflate(inflater)
        // Inflate the layout for this fragment
        return binding.root
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if( context is OpenNew)
            interf = context

        val callback: OnBackPressedCallback = object : OnBackPressedCallback(
            true // default to enabled
        ) {
            override fun handleOnBackPressed() {
                if(binding.webItem.canGoBack()) binding.webItem.goBack()
                else {
                    interf.back()
                }

            }
        }
        requireActivity().onBackPressedDispatcher.addCallback(
            this,  // LifecycleOwner
            callback
        )
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        web_setUp()
    }



    @SuppressLint("SetJavaScriptEnabled")
    @RequiresApi(Build.VERSION_CODES.O)
    private fun web_setUp(){
        binding.webItem.webViewClient = WebViewClient()

        binding.webItem.apply {
            loadUrl("https://www.google.com/")
            settings.javaScriptEnabled = true
            settings.safeBrowsingEnabled = true
            urlFromUser = url.toString()
        }
    }




//    override fun onBackPressed() {
//        if(binding.webItem.canGoBack()) binding.webItem.goBack() else Navigation.findNavController(binding.root).navigate(R.id.selfWeb)
//    }

    companion object {
        @JvmStatic
        fun newInstance() = WebFragment()

    }
}