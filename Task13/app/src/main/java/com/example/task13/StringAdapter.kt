package com.example.task13

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.task13.databinding.StringPatternBinding
import kotlin.random.Random

class StringAdapter:RecyclerView.Adapter<StringAdapter.StringHolder>() {
    val stringList = ArrayList<OneString>()
    class StringHolder(item:View):RecyclerView.ViewHolder(item){
        val binding = StringPatternBinding.bind(item)
        fun bind(string:OneString) = with(binding){
            when(rand()){
                1->textView.setTextAppearance(binding.root.context,  R.style.MyStyle1)
                2->textView.setTextAppearance(binding.root.context,  R.style.MyStyle2)
                3->textView.setTextAppearance(binding.root.context,  R.style.MyStyle3)
            }
            textView.text = string.title
        }
        private fun rand():Int{
            return Random.nextInt(1, 3)
        }
    }




    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): StringHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.string_pattern, parent, false)
        return StringHolder(view)
    }

    override fun onBindViewHolder(holder: StringHolder, position: Int) {
        holder.bind(stringList[position])
    }

    override fun getItemCount(): Int {
        return stringList.size
    }

    fun addString(string:OneString){
        stringList.add(string)
        notifyDataSetChanged()
    }
}